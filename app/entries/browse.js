var axios = require('axios');
var React = require('react');
var ReactDOM = require('react-dom');

var Item = React.createClass({
    getInitialState: function () {
        return {
            favourited: false
        }
    },
    componentDidMount: function () {
        if (localStorage[this.props.id] !== undefined) {
            var newState = localStorage[this.props.id] == 'true';
            this.setState({
                favourited: newState
            });
        }

    },
    isFavourited: function () {
        return this.state.favourited === true || this.state.favourited === 'true' ? '■' : '□'
    },
    toggleFav: function () {
        localStorage.setItem(this.props.id, !this.state.favourited);
        this.setState({
            favourited: !this.state.favourited
        });
    },
    render: function () {
        var price = this.props.price !== null ? this.props.price.amounts['USD'] : '';

        return <div className="item">
            <div className="container">
                <a href={'item/' + this.props.id}>
                    <img src={this.props.image} alt={this.props.title}/>
                </a>
                <div className="info-line">
                    <div className="left"><span>{price}</span></div>
                    <div className="right"><a className="fav" onClick={this.toggleFav}>{this.isFavourited()}</a></div>
                </div>
            </div>
        </div>
    }
});

var ProductList = React.createClass({
    getInitialState: function () {
        return {
            itemList: [],
            count: 3,
            loaded: 0
        }
    },
    componentDidMount: function () {
        this.loadItems(this.state.count);
        var updateLoaded = this.state.loaded + this.state.count;
        this.setState({
            loaded: updateLoaded
        });
    },
    loadItems: function (count) {
        axios('/data', {
            params: {
                limit: count
            }
        })
            .then((function (response) {
                this.setState({
                    itemList: response.data.items
                });
                this.forceUpdate();
            }).bind(this));

    },
    loadMore: function () {
        this.loadItems(this.state.loaded + this.state.count);
        this.setState({
            loaded: this.state.loaded + this.state.count
        });
    },
    render: function () {
        var items = this.state.itemList.map(function (item) {
            if (item) {
                return <Item id={item.id} key={item.id} price={item.price} image={item.image}></Item>
            }
        });
        return <div className="item-list">
            {items}
            <div className="list-bottom">
                <button onClick={this.loadMore}>Load more</button>
            </div>
        </div>;
    }
});


ReactDOM.render(<ProductList></ProductList>, document.getElementById('list'));