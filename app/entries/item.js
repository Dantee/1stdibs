var axios = require('axios');
var React = require('react');
var ReactDOM = require('react-dom');

var ItemPreview = React.createClass({
    getInitialState: function () {
        return {
            item: {},
            favourited: false
        }
    },
    componentDidMount: function () {
        var url = window.location.pathname + '/data';
        axios(url)
            .then((function (response) {
                this.setState({
                    item: response.data
                });

                if (localStorage[response.data.id] !== undefined) {
                    var newState = localStorage[response.data.id] == 'true';
                    this.setState({
                        favourited: newState
                    });
                }
            }).bind(this));
    },
    isFavourited: function () {
        return this.state.favourited === true || this.state.favourited === 'true' ? '■' : '□'
    },
    toggleFav: function () {
        localStorage.setItem(this.state.item.id, !this.state.favourited);
        this.setState({
            favourited: !this.state.favourited
        });
    },
    render: function () {
        var item = this.state.item;
        var price = item.price !== undefined && item.price !== null ? item.price.amounts['USD'] : '';
        var measurements = item.measurements !== undefined ? item.measurements.display : '';
        return <div className="item-preview">
            <div className="left">
                <div className="block">
                    <div className="fav">
                        <a onClick={this.toggleFav}>{this.isFavourited()}</a>
                    </div>
                    <img src={item.image} alt={item.title}/>
                </div>
            </div>
            <div className="right">
                <div className="block">
                    <div className="padding">
                        <h2>{item.title}</h2>
                        <div className="price">
                            {price}
                        </div>
                        <div>
                            Measurements: {measurements}
                        </div>
                    </div>
                    <div className="buttons">
                        <a href="#">Purchase</a>
                        <a href="#">Make offer</a>
                    </div>
                </div>
                <div className="block">
                    <div className="padding">
                        <div className="description">
                            {item.description}
                        </div>
                        <div className="creator">
                            {item.creators}
                        </div>
                    </div>
                </div>
            </div>
        </div>;
    }
});

ReactDOM.render(<ItemPreview></ItemPreview>, document.getElementById('item'));
